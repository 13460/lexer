					       COMPILER DESIGN(CS335A)
 			Amit Kumar (13095)  
			Pankaj kumar singh (13460)
			Shiv shankar Azad (13655) 
			Tarun kuamr (13742)


We had made a lexer for Rust implementated in Python.

It returns the lexemes for the given rust programme.
This submission has three basic files lexer.py, lex.py 
and Makefile.							resources:http://www.dabeaz.com/ply/ply.html#ply_nn3      
							        (  that contains whole documentation of ply) 

To run the code : make testX.rs (X=1,2,3,4,5)

We had taken reserved words used in the language in a dictionary and taken tokens names
for all operators, idenfiers, string,chars, integers and special symbols and  
implimented their regular expressions to match the patterns in lexer.py.

Power operator:=Rust does not have power operator  libcore has a 
float::pow_with_uint function:

tool used : PLY  which is combination of lexer and parser
