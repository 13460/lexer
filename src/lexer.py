#!/usr/bin/python
import lex

reserved = {
	'as' : 'AS',
    	'break' : 'BREAK',
    	'crate' : 'CRATE',
    	'else' : 'ELSE',
    	'enum' : 'ENUM',
    	'extern' : 'EXTERN',
    	'false' : 'FALSE',
    	'fn' : 'FUNCTION',
    	'for' : 'FOR_LOOP',
    	'if' : 'IF',
    	'impl' : 'IMPL',
    	'in' : 'IN',
    	'let' : 'LET_LOOP',
    	'loop' : 'LOOP',
	'main' : 'MAIN_FUN',
    	'match' : 'MATCH',
    	'mod' : 'MOD',
    	'move' : 'MOVE',
    	'mut' : 'MUT',
    	'pub' : 'PUB',
	#"println+!" : 'PRINTF_FUN',
    	'ref' : 'REF',
    	'return' : 'RETURN',
    	'static' : 'STATIC',
    	'selfValue' : 'SELFVALUE',
    	'selfType' : 'SELFTYPE',
    	'struct' : 'STRUCT',
    	'super' : 'SUPER',
    	'true' : 'TRUE',
    	'trait' : 'TRAIT',
    	'type' : 'TYPE',
    	'unsafe' : 'UNSAFE',
    	'use' : 'USE',
    	'virtual' : 'VIRTUAL',
    	'while' : 'WHILE_LOOP',
    	'continue' : 'CONTINUE',
    	'box' : 'BOX',
    	'const' : 'CONST',
    	'where' : 'WHERE',
    	'proc' : 'PROC',
    	'alignof' : 'ALIGNOF',
    	'become' : 'BECOME',
    	'offsetof' : 'OFFSETOF',
    	'priv' : 'PRIV',
    	'pure' : 'PURE',
    	'sizeof' : 'SIZEOF',
    	'typeof' : 'TYPEOF',
    	'unsized' : 'UNSIZED',
    	'yield' : 'YIELD',
    	'do' : 'DO',
    	'abstract' : 'ABSTRACT',
    	'final' : 'FINAL',
    	'override' : 'OVERRIDE',
    	'macro' : 'MACRO'
		}





tokens = [
	'PRINT',
	'SENTENCE',
	'IDENTIFIER',
	'NUMBER',
	'LESSEQUAL',
	'ASSIGN',
	'BEGIN_BLOCK',
	'END_BLOCK',
	'OPEN_PARANTHESIS',
	'CLOSE_PARANTHESIS',
	'SEMICOLON',
	'COMMA',
	'MINUS',
	'NOTEQUAL',
	'PLUS',
     	'STRING',
	'MULTIPLICATION',
	'DIVISION',
	'COLON',
	'FLOAT',
	'AND_OP',
	'AND_LOGICAL',
	'OR_OP',
	'OR_LOGICAL',
	'XOR_OP',
	'NOT_OP',
	'LEFT_SHIFT',
	'RIGHT_SHIFT',
	'EQUAL_COMP',
	'LESS_THAN',
	'GREATER_THAN',
	'MODULUS',
	'OPEN_BRACKET',
	'CLOSE_BRACKET',
	'DOTDOT',
	'COMMENT',
	
] + list(reserved.values())


count={

	'print':'0',
	'sentence':'0',
	'identifier':'0',
	'number':'0',
	'lessequal':'0',
	'assign':'0',
	'beginblock':'0',
	'endblock':'0',
	'openp':'0',
	'closep':'0',
	'semi':'0',
	'comma':'0',
	'minus':'0',
	'not':'0',
	'plus':'0',
     	'string':'0',
	'multi':'0',
	'div':'0',
	'colon':'0',
	'float':'0',
	'and':'0',
	'andl':'0',
	'orop':'0',
	'orlog':'0',
	'xorop':'0',
	'notopt':'0',
	'lefts':'0',
	'rights':'0',
	'equalco':'0',
	'lessth':'0',
	'greatth':'0',
	'mudu':'0',
	'openb':'0',
	'closebr':'0',
	'dotdot':'0',
	'comment':'0'
}




t_ignore_WHITESPACE=r"\s"

def t_COMMENT(t):
	r'//'
	return t

def t_DOTDOT(t):
	r'\.\.'
	return t

def t_PRINT(t):
	r"\println!"	
	return t

def t_SENTENCE(t):
	r'\"(.+?)\"'
	return t

def t_IDENTIFIER(t):
    r"[&]?[a-zA-Z$_][\w$]*"
    t.type = reserved.get(t.value,'IDENTIFIER')    
    return t

def t_NUMBER(t):
    r"\d+"
    t.value = int(t.value)
    return t

def t_LESSEQUAL(t):				
	r"<=|"r"le"
	return t


def t_ASSIGN(t):
	r"="
	return t

def t_BEGIN_BLOCK(t):
	r"\{"
	return t

def t_END_BLOCK(t):
	r"\}"
	return t


def t_OPEN_PARANTHESIS(t):
	r"\("
	return t

def t_CLOSE_PARANTHESIS(t):
	r"\)"
	return t

def t_SEMICOLON(t):
	r";"
	return t

def t_COMMA(t):
	r","
	return t

def t_MINUS(t):
	r"-"
	return t

def t_NOTEQUALS(t):
	r"\!=|"r"ne"
	return t

def t_PLUS(t):
	r"\+"
	count['plus']+=1
	return t

def t_STRING(t):
	r"\'([^\'])*\'"
	return t

def t_MULTIPLICATION(t):
	r"\*"
	return t

def t_DIVISION(t):
	r"/"
	return t

def t_COLON(t):
	r"\:"
	return t

def t_FLOAT(t):
    r"\d+\.\d+"
    t.value = float(t.value)
    return t

def t_AND_OP(t):
	r"&\&"
	return t

def t_AND_LOGICAL(t):
	r"\&"
	return t

def t_OR_OP(t):
	r"\|\|"
	return t

def t_OR_LOGICAL(t):
	r"\|"
	return t


def t_XOR_OP(t):
	r"\^"
	return t

def t_NOT_OP(t):
	r"\!|"r"not"
	return t

def t_LEFT_SHIFT(t):
	r"<<"
	return t

def t_RIGHT_SHIFT(t):
	r">>"
	return t

def t_EQUALS_COMP(t):
	r"==|"r"eq"
	return t

def t_LESS_THAN(t):
	r"<|"r"lt"
	return t

def t_GREATER_THAN(t):				
	r">|"r"gt"
	return t

def t_MODULUS(t):
	r"%"
	return t

def t_OPEN_BRACKET(t):
	r"\["
	return t

def t_CLOSE_BRACKET(t):
	r"\]"
	return t

#error	
def t_error(t):
	print "Illegal character %s" % t.value[0]
	t.lexer.skip(1)


lexer=lex.lex()
def runlexer(inputfile):
	program=open(inputfile).read()
	lexer.input(program)
	line1 = ""
	line2 = "#"
	LineNum = 0;
	for tok in iter(lexer.token, None):
		
		while tok.lineno!=LineNum:

		 	LineNum+=1
		 	if (line1 != ""):
				print "%s\n%s\n" %(line1, line2)
				print "print",tokens[0]
				#print  "print",count['plus']
			#else:
				#print "\n"
			line1 = ""
			line2 = "#"
		line1 += " %s" %(tok.value)
		line2 += " %s" %(repr(tok.type))
	print "%s\n%s\n" %(line1, line2)
	#print count['plus']
if __name__=="__main__":
	from sys import argv 
	filename, inputfile = argv
	runlexer(inputfile)
